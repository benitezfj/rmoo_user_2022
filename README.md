# useR! 2022

rmoo package presentation for useR! conference - June/2022


![](./img/poster.png)


## Contents

* Multi-Objective Optimization Concept
* Industrial Application
* Background
* Overview
* Installation
* Organization
* Main Functions
* Workflow
* Application Example
    - Part. 1 & 2
    - Part. 3
    - Part. 4.1
    - Part. 4.2
* Contacts    
* References
